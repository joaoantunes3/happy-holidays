import {Component, OnInit} from '@angular/core';
import * as _ from 'lodash';
import {CalendarService, HolidayInfo} from './calendar.service';
import { ReplaySubject } from 'rxjs';
import { HolidayFiltersType } from 'src/holiday-filters/holiday-filters.component';


export enum LastOrNext {
    Last = 'last',
    Next = 'next'
}

export class CalendarDay {
    public date: Date;
    public title: string = '';
    public isPastDate: boolean;
    public isToday: boolean;
    public holidays: HolidayInfo[] = [];

    constructor(d: Date) {
        this.date = d;
        const [receivedDateMidnight, currDateMidnight] = [d.setHours(0, 0, 0, 0), new Date().setHours(0, 0, 0, 0)];
        this.isPastDate = receivedDateMidnight < currDateMidnight;
        this.isToday = receivedDateMidnight === currDateMidnight;
    }
}

@Component({
    selector: 'hh-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
    public currCalendarDays: CalendarDay[] = [];
    public matchedHolidays: HolidayInfo[] = [];
    public lastOrNext = LastOrNext;
    public weekDayNames: string[] = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    public monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public firstDateFromRange$ = new ReplaySubject<Date>(1);
    private _filterCountries: string[] = [];

    constructor(private _calendarService: CalendarService) { }

    async ngOnInit(): Promise<void> {
        this.currCalendarDays = this._calendarService.getCalendarDays(this.getFirstMondayFromDate(new Date()));
    }

    getFirstMondayFromDate(origin: Date, lastOrNext: LastOrNext = LastOrNext.Last): Date {
        origin.setDate(1);
        while(origin.getDay() !== 1) {
            origin.setDate(origin.getDate() - 1);
        }
        return origin;
    }

    changeMonthOrYear(lastOrNext?: LastOrNext, changeMonth = true): void {
        const originDate = _.isNil(lastOrNext)
            ? new Date()
            : lastOrNext === LastOrNext.Last
                ? changeMonth
                    ? this._calendarService.addDaysToDate(this.currCalendarDays[0].date, -1)
                    : this._calendarService.addDaysToDate(this.currCalendarDays[Math.ceil(this.currCalendarDays.length / 2)].date, -365)
                : changeMonth
                    ? this._calendarService.addDaysToDate(this.currCalendarDays[this.currCalendarDays.length - 1].date, 1)
                    : this._calendarService.addDaysToDate(this.currCalendarDays[Math.ceil(this.currCalendarDays.length / 2)].date, 365);
        this.currCalendarDays = this._calendarService.getCalendarDays(this.getFirstMondayFromDate(originDate, lastOrNext));
        this._updateCalendarHolidays();
        this.firstDateFromRange$.next(this.currCalendarDays[0].date);
    }

    handleFilterUpdate(filterValues: HolidayFiltersType): void {
        this._filterCountries = filterValues.countries;
        this._updateCalendarHolidays();
    }

    private async _updateCalendarHolidays() {
        this.matchedHolidays = await this.getCurrHolidays();
        _.forEach(this.currCalendarDays, day => { 
            day.holidays = _.filter(this.matchedHolidays, ['date', day.date.toISOString().split('T')[0]]);
        });
    }

    shouldDisplayMonth(c: CalendarDay, weekIndex: number, dayIndex: number): boolean {
        return c.isToday || (!weekIndex && !dayIndex) || (c.date.getDate() === 1);
    }

    getCurrHolidays(): Promise<HolidayInfo[]> {
        return this._calendarService.filterHolidays(this._filterCountries,
            _.uniq([this.currCalendarDays[0].date.getFullYear(),
                this.currCalendarDays[this.currCalendarDays.length - 1].date.getFullYear()]) as number[]);
    }
}
