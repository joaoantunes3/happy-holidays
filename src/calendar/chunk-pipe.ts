import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {CalendarDay} from './calendar.component';


@Injectable()
@Pipe({name: 'chunkPipe'})
export class ChunkPipe implements PipeTransform {
    transform(calendarDaysArray: any, chunkSize: number) {
        let calendarDays: CalendarDay[][] = [];
        let weekDays: CalendarDay[] = [];

        calendarDaysArray.map((day: CalendarDay,index: number) => {
            weekDays.push(day);
            if (++index % chunkSize === 0) {
                calendarDays.push(weekDays);
                weekDays = [];
            }
        });
        return calendarDays;
    }
}
