import {CalendarDay} from './calendar.component';
import {HttpClient} from '@angular/common/http';
import {first} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import * as _ from 'lodash';
import type {Dictionary, NumericDictionary} from 'lodash';
import {Subject, from} from 'rxjs';


export type IsoCountryType = {name: string, 'alpha-2': string, 'country-code': string};

export type HolidayInfo = {
    date: string,
    localName: string,
    name: string,
    countryCode: string,
    fixed: boolean,
    global: boolean,
    counties: boolean
    launchYear: number,
    types: string[]
}

@Injectable({
    providedIn: 'root',
})
export class CalendarService {
    public updatedDay = new Subject<CalendarDay>();
    private _isoCountryCodes: IsoCountryType[] = [];
    private _fetchedHolidays: NumericDictionary<Dictionary<HolidayInfo[]>> = {}; // {2023: {uk: []}}
    private _calendarDays: CalendarDay[] = [];
    private _nDaysPerMonth = 42;

    constructor(private httpClient: HttpClient) {
    }

    getCalendarDays(originDate: Date): CalendarDay[] {
        const [from, to] = [originDate, this.addDaysToDate(originDate)];
        const datesRange = this.getDatesRange(from, to);
        // If you find boths dates, it's safe to assume the dates in between were already fetched as well
        if (_.find(this._calendarDays, ['date', from]) && _.find(this._calendarDays, ['date', to])) {
            return _.map(datesRange, date => _.find(this._calendarDays, ['date', date])) as CalendarDay[];
        }

        const calendarDays: CalendarDay[] = _.map(datesRange, date => new CalendarDay(date));
        this._calendarDays.push(...calendarDays);
        return calendarDays;
    }

    getDatesRange(from: Date, to: Date): Date[] {
        const dateArray = new Array();
        let currentDate = from;
        while (currentDate <= to) {
            dateArray.push(currentDate);
            currentDate = this.addDaysToDate(currentDate, 1);
        }
        return dateArray;
    }

    addDaysToDate(originDate: Date, nDays = this._nDaysPerMonth - 1): Date {
       let newDate = new Date(originDate);
       newDate.setDate(newDate.getDate() + nDays);
       return newDate;
    }

    private async _requestIsoCountryCodes(): Promise<IsoCountryType[]> {
        this._isoCountryCodes = await this.httpClient.get('./../assets/iso-3166-countries-with-regional-codes.json')
            .pipe(first()).toPromise() as IsoCountryType[];
        return this._isoCountryCodes;
    }

    async getIsoCountryCodes(): Promise<IsoCountryType[]> {
        return this._isoCountryCodes.length ? from(Promise.resolve(this._isoCountryCodes)).toPromise() : await this._requestIsoCountryCodes();
    }

    async filterHolidays(selectedCountries: string[], selectedYears: number[]): Promise<HolidayInfo[]> {
        const requestedHolidays = [];
        for (const year of selectedYears) {
            for (const country of selectedCountries) {
                const alreadyFetched = _.has(this._fetchedHolidays, `${year}.${country}`);
                let holidays: HolidayInfo[] = [];
                try {
                    holidays = (alreadyFetched
                        ? _.get(this._fetchedHolidays, `${year}.${country}`)
                        : (await this.httpClient.get(`https://date.nager.at/api/v3/publicholidays/${year}/${country}`)
                            .pipe(first()).toPromise()) as HolidayInfo[]) || [];
                } catch (error) { }
                !alreadyFetched && _.set(this._fetchedHolidays, `${year}.${country}`, holidays);
                requestedHolidays.push(...holidays);
            }
        }
        return requestedHolidays;
    }
}
