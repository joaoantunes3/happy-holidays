import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DayInfoComponent } from '../day-info/day-info.component';
import { CalendarComponent } from '../calendar/calendar.component';

const routes: Routes = [
    { path: 'calendar', component: CalendarComponent },
    { path: 'dayInfo', component: DayInfoComponent },
    { path: '**', redirectTo: '/calendar', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
