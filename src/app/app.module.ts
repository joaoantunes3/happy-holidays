import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CalendarComponent} from '../calendar/calendar.component';
import {ChunkPipe} from '../calendar/chunk-pipe';
import {HttpClientModule} from '@angular/common/http';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MaterialModule} from './material.module';
import {FormsModule} from '@angular/forms';
import { HolidayFiltersComponent } from '../holiday-filters/holiday-filters.component';
import { DayInfoComponent } from '../day-info/day-info.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    DayInfoComponent,
    HolidayFiltersComponent,
    ChunkPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    FormsModule
  ],
  providers: [MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
