import { Component, Input, OnDestroy, OnInit, Output } from "@angular/core";
import * as _ from 'lodash';
import { ReplaySubject, Subscription } from "rxjs";
import { CalendarService, IsoCountryType } from "src/calendar/calendar.service";

export type HolidayFiltersType = {countries: string[], currentMonth?: string, startDate?: Date, endDate?: Date};

@Component({
    selector: 'holiday-filters',
    templateUrl: './holiday-filters.component.html',
    styleUrls: ['./holiday-filters.component.scss']
})
export class HolidayFiltersComponent implements OnDestroy, OnInit {
    isoCountries: IsoCountryType[] = [];
    filter: HolidayFiltersType = {countries: []};
    @Output() filterUpdated = new ReplaySubject<HolidayFiltersType>(1);
    @Input() firstDateFromRange$: ReplaySubject<Date>;
    _enableDateRange = false;
    @Input() set enableDateRange(enable: boolean) {
        this._enableDateRange = enable;
        if (!enable) {
            this.filter.currentMonth = this._getCurrentMonth(new Date(new Date().getFullYear(), new Date().getMonth(), 1));
            return;
        }

        const today = new Date();
        this.filter.startDate = new Date(today.getFullYear(), 0, 1);
        this.filter.endDate = new Date(today.getFullYear(), 11, 31);
    }
    private _subscription: Subscription;
    

    constructor(private _calendarService: CalendarService) {
    }

    async ngOnInit() {
        // This only gets used on the calendar view
        this._subscription = this.firstDateFromRange$?.subscribe((startDate) => {
            this.filter.currentMonth = this._getCurrentMonth(startDate);
        });
        this.isoCountries = await this._calendarService.getIsoCountryCodes();
    }

    ngOnDestroy() {
        this._subscription?.unsubscribe();
      }

    onApply() {
        this.filterUpdated.next(this.filter);
    }


    getToolTipData(isoCountries: string[]) {
        const val = _.map(isoCountries, iso => _.find(this.isoCountries, ['alpha-2', iso])?.name || '').join(', ');
        return val;
    }
    private _getCurrentMonth(date: Date): string {
        const firstMonthDate = new Date(date.getFullYear(), date.getMonth() + (date.getDate() === 1 ? 0 : 1), 1)
        return firstMonthDate.toLocaleString('default', { month: 'long', year: "numeric" });  
    }
}