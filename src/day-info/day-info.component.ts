import { Component, ElementRef, ViewChild } from '@angular/core';
import { CalendarService, HolidayInfo } from '../calendar/calendar.service';
import * as _ from 'lodash';
import { from } from 'rxjs';
import { HolidayFiltersType } from '../holiday-filters/holiday-filters.component';

@Component({
    selector: 'hh-day-info',
    templateUrl: './day-info.component.html',
    styleUrls: ['./day-info.component.scss']
})
export class DayInfoComponent {
    @ViewChild('filterArea') filterArea: ElementRef;
    matchedHolidays: HolidayInfo[] = [];
    private _filterValues: HolidayFiltersType;

    constructor(private _calendarService: CalendarService) {
    }


    handleFilterUpdate(filterValues: HolidayFiltersType) {
        this._filterValues = filterValues;
        this._updateCalendarHolidays();
    }

    private async _updateCalendarHolidays() {
        const matchedHolidays = await this.getCurrHolidays();
        this.matchedHolidays = _.filter(matchedHolidays, holiday => {
            const holidayDate = new Date(holiday.date);
            return !!this._filterValues.startDate && !!this._filterValues.endDate &&
                holidayDate >= this._filterValues.startDate && holidayDate < this._filterValues.endDate
        });
    }
    
    getCurrHolidays(): Promise<HolidayInfo[]> {
        return _.some([this._filterValues.startDate, this._filterValues.endDate], d => _.isNil(d))
            ? from(Promise.resolve([] as HolidayInfo[])).toPromise()
            : this._calendarService.filterHolidays(this._filterValues.countries,
                _.uniq([this._filterValues.startDate?.getFullYear(), this._filterValues.endDate?.getFullYear()]) as number[]);
    }

    getHolidayCardsStyle(): Object {
        const filterAreaHeight = this.filterArea?.nativeElement.clientHeight;
        return filterAreaHeight ? {'height': `calc(100% - ${filterAreaHeight}px)`} : {};
    }

    formatTitle(key: string): string {
        return _.chain(key)
            .toLower()
            .split('_')
            .map(word => word.charAt(0).toUpperCase() + word.slice(1))
            .join(' ')
            .value();
    }
}
