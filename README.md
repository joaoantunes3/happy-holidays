# HappyHolidays

An Angular calendar project constructed using standard HTML tables, allowing the display of national holidays for any chosen country.

Navigate to the project's folder and run `npm i` to install the required dependencies.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/` and the app will load.